package service;

import model.Message;
import model.User;
import interfaces.MessageInterface;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import dao.MessageDao;

@Path("/MessageService")
@Api(value="/MessageService")
public class MessageService implements MessageInterface {

	MessageDao dao  = new MessageDao();
	
	@Path("/send")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Use when user wants to send message",notes="")
	public String sendMessage(Message message) {
		// TODO Auto-generated method stub
		return dao.sendMessage(message);
	}

	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Used to get all messages of user",notes="sender_id will be id of logged in user and "
			+ "receiver_id will be id of other user."
			+ "Messages will be returned in order ",response=Message.class,responseContainer="List")
	public String getAllMessages(@QueryParam("sender_id")int sender_id,@QueryParam("receiver_id")int receiver_id) {
		// TODO Auto-generated method stub
		return dao.getAllMessages(sender_id, receiver_id);
	}

}
