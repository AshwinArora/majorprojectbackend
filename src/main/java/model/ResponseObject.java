package model;

public class ResponseObject<T> {
	Boolean success;
	String errorMsg;
	T data;
	
	public ResponseObject(){
		this.success = true;
		this.errorMsg = "";
	}

	public ResponseObject(Boolean success, String errorMsg, T data) {
		this.success = success;
		this.errorMsg = errorMsg;
		this.data = data;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	
	
}
