package interfaces;

import java.math.BigDecimal;

import model.Login;

import model.User;

public interface UserInterface {
	
	public String userLogin(Login data);
	public String getUser(User  user);
	public String updateUser(User data);
	public String createUser(User data);
	public String addInterests(User data);
	public String getNearbyUsers(BigDecimal latitude,BigDecimal longitude,BigDecimal radius,int user_id);
	public String getRecommendedUsers(BigDecimal latitude,BigDecimal longitude,BigDecimal radius,int user_id);
	//public String getInterests();
	public String getCategoryAndInterests();
	public String userLikes(int userId,int userLikedId);
	public String getMatchedUserList(int id);
	public String updateLocation(BigDecimal latitude,BigDecimal longitude,int user_id);
}
