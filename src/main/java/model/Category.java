package model;

import java.util.ArrayList;

import io.swagger.annotations.ApiModel;

@ApiModel
public class Category {
	int category_id;
	String category_name;
	ArrayList<Interest> interests;
	
	public Category(){
		this.interests = new ArrayList<>();
	}
	
	public Category(int category_id, String category_name,
			ArrayList<Interest> interests) {
		this.category_id = category_id;
		this.category_name = category_name;
		this.interests = interests;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public ArrayList<Interest> getInterests() {
		return interests;
	}
	public void setInterests(ArrayList<Interest> interests) {
		this.interests = interests;
	}
	
	
}
