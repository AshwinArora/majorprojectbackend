package model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.ArrayList;

@ApiModel
public class User {
	Integer id;
	String name;
	String email_id;
	String number;
	@ApiModelProperty(hidden=true)
	String password;
	BigDecimal longitude;
	BigDecimal latitude;
	ArrayList<Interest> interests;

	public User(){
		this.id = -1;
		this.interests = new ArrayList<>();
	}

	public User(Integer id, String name, String email_id, String number,
			String password, BigDecimal longitude, BigDecimal latitude,
			ArrayList<Interest> interests) {
		this.id = id;
		this.name = name;
		this.email_id = email_id;
		this.number = number;
		this.password = password;
		this.longitude = longitude;
		this.latitude = latitude;
		this.interests = interests;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public ArrayList<Interest> getInterests() {
		return interests;
	}
	public void setInterests(ArrayList<Interest> interests) {
		this.interests = interests;
	}



}
