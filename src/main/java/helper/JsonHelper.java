package helper;

import com.google.gson.Gson;

public class JsonHelper {
	public static String toJsonString(Object o){
		return new Gson().toJson(o);
	}

}
