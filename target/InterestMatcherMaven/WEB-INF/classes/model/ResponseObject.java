package model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ResponseObject {
	Boolean success;
	String errorMsg;
	JsonElement data;
	
	public ResponseObject(){
		this.success = true;
		this.errorMsg = "";
	}
	
	public ResponseObject(Boolean success, String errorMsg, JsonObject data) {
		this.success = success;
		this.errorMsg = errorMsg;
		this.data = data;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public JsonElement getData() {
		return data;
	}
	public void setData(JsonElement data) {
		this.data = data;
	}
	
	public void setAndConvert(Object o){
		String json  = new Gson().toJson(o);
		this.data = new Gson().toJsonTree(json).getAsJsonObject();
	}
}
