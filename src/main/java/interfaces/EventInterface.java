package interfaces;

import java.math.BigDecimal;

import model.Event;

public interface EventInterface {
	
	public String getAllEventByLocation(BigDecimal latitude,
			BigDecimal longitude,BigDecimal radius);
	public String getEvent(int id);
	public String getEventByUserId(int id);
	public String createEvent(Event eventData);
	public String updateEvent(Event eventData);
	public String deleteEvent(int id);
	public String subscribeToEvent(int user_id,int event_id);
	public String unsubscribeToEvent(int user_id,int event_id);
	
}
