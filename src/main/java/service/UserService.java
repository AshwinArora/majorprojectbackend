package service;

import java.math.BigDecimal;

import interfaces.UserInterface;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import model.Category;
import model.Login;
import model.User;
import dao.UserDao;

@Path("/UserService")
@SwaggerDefinition(info = @Info(title = "Interest Matcher", version = "1.0",description="All responses will be of the following format - { \"success\":boolean , \"errorMsg\":String , \"data\": JsonObject\\JsonArray as defined in response of each api }"
		+ " The data key will contain the response for the api which are defined below.ResponseObject is basically a wrapper for all responses. First check success key to see if api operations have been successful. Input json has been defined for each api but for input send only what is required for eg for userLogin api send only email_id and password"))           
@Api(value="UserService")
public class UserService implements UserInterface{

	@Path("/userSignUp")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Allows User to sign up",notes="Input data is same as response:",response = User.class)
	public String createUser(User data){
		UserDao dao = new UserDao();

		return dao.createUser(data);
	}

	@Path("/userLogin")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Allows User to login",notes="send json-  { email_id: string , password: string}",response = User.class)
	//@ApiImplicitParams({@ApiImplicitParam(name="user_email_id",value="User email id",dataType="String",paramType="body")})
	public String userLogin(Login data){
		UserDao dao = new UserDao();
		return dao.userLogin(data); 
	}

	@Path("/getUser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Used to get user using id",notes="input json - { id : integer } ",response = User.class)
	public String getUser(User data){
		UserDao dao = new UserDao();
		String resp =dao.getUser(data);
		System.out.println("GetUser : "+ resp);
		return resp;
	}

	@Path("/editUser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Allows you to update user properties",notes="Send all the data given below regardless of whether it was updated or not.If you send anything different then it will get updated.Use this api for location updates also.",response = User.class)
	public String updateUser(User data){
		UserDao dao = new UserDao();
		return dao.updateUser(data);
	}

	@Path("/addInterests")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Interests are added according to user id.",notes= "Only use when signUp is occuring. No data is returned. Input json is array of Interests Model")
	public String addInterests(User data) {
		UserDao dao = new UserDao();
		return dao.addInterests(data);
	}

	@Path("/getNearbyUsers/{user_id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Finds nearby users in a specified radius.Working now.  ",
	response = User.class,responseContainer = "List")
	public String getNearbyUsers(@ApiParam(value="latitude of user") 
			@QueryParam("latitude")BigDecimal latitude,
			@ApiParam(value="longitude of user")
			@QueryParam("longitude")BigDecimal longitude,
			@ApiParam(value="radius provided by user") 
			@QueryParam("radius") BigDecimal radius,
			@ApiParam(value="id of user") @PathParam("user_id") int user_id) {
		UserDao dao = new UserDao();
		System.out.println("Nearby");
		return dao.getNearbyUsers(latitude,longitude,radius,user_id);
	}

	@Path("/getRecommendedUsers/{user_id}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Working Now. Shows users based on their "
			+ "dice distance from current user")
	public String getRecommendedUsers(@ApiParam(value="latitude of user") 
	@QueryParam("latitude")BigDecimal latitude,
	@ApiParam(value="longitude of user")
	@QueryParam("longitude")BigDecimal longitude,
	@ApiParam(value="radius provided by user") 
	@QueryParam("radius") BigDecimal radius,
	@ApiParam(value="id of user") @PathParam("user_id") int user_id){
		UserDao dao = new UserDao();
		return dao.getRecommendedUsers(latitude,longitude,radius,user_id);
	}

/*	@Path("getInterests")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Return all categories and interests",notes="input json - { category : \"array of category objects\", interest:\"array of interest objects\" ")
	public String getInterests() {
		UserDao dao = new UserDao();
		return dao.getInterests();
	}*/

	@Path("/getCategory")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Return all categories and interests",response=Category.class,responseContainer="List")
	public String getCategoryAndInterests() {
		UserDao dao = new UserDao();
		return dao.getCategoryAndInterests();
	}
	
	@Path("/test")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "test")
	@ApiImplicitParams(value={
	        @ApiImplicitParam(name="val1", value="Input 1", dataType="string", paramType="body"),
	        @ApiImplicitParam(name="val2", value="Input 2", dataType="string", paramType="body")
	})
	public String test() {
		UserDao dao = new UserDao();
		return dao.getCategoryAndInterests();
	}
	
	@Path("/{user_id}/likes/{user_liked_id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Use when logged in user likes another user"
			+ " i.e user swipes right on another user ",
			notes = "Json will be of following format - "
			+ "{ \"isMatch\":boolean } ")
	public String userLikes(@PathParam("user_id")int userId,@PathParam("user_liked_id")int userLikedId){
		UserDao dao = new UserDao();
		return dao.userLikes(userId, userLikedId);
	}

	@Path("/getMatchedUsers/{user_id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Use to get list of matched users",response=User.class,responseContainer="List")
	public String getMatchedUserList(@PathParam("user_id")int id){
		UserDao dao = new UserDao();
		return dao.getMatchedUserList(id);
	}
	
	@Path("/update/{user_id}/location")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="Update user location")
	public String updateLocation(@ApiParam(value="latitude of user") 
	@QueryParam("latitude")BigDecimal latitude,
	@ApiParam(value="longitude of user")
	@QueryParam("longitude")BigDecimal longitude,
	@ApiParam(value="id of user") @PathParam("user_id") int user_id){
		return new UserDao().updateLocation(latitude, longitude, user_id);
	}

	

}
