package model;

public class Recommended implements Comparable<Recommended>{
	int user_id;
	int[] user_interests;
	double similarity;
	
	public Recommended(){
		this.user_interests = new int[12];
	}
	
	public void addInterestPresent(int index){
		this.user_interests[index] = 1;
	}

	
	public Recommended(int user_id, int[] user_interests, double similarity) {
		this.user_id = user_id;
		this.user_interests = user_interests;
		this.similarity = similarity;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int[] getUser_interests() {
		return user_interests;
	}

	public void setUser_interests(int[] user_interests) {
		this.user_interests = user_interests;
	}

	public double getSimilarity() {
		return similarity;
	}

	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}

	@Override
	public int compareTo(Recommended o) {
		double otherSimilarity = o.getSimilarity();
		return Double.compare(this.getSimilarity(), otherSimilarity);
	}
	
	
}
