package interfaces;

public interface UserInterface {
	
	public String userLogin(String data);
	public String getUser(String data);
	public String updateUser(String data);
	public String createUser(String data);
	public String addInterests(String data);
	public String getNearbyUsers(String data);
	public String getRecommendedUsers(String data);

}
