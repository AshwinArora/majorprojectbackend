package model;

public class Message {
	int senderId;
	int receiverId;
	String message;
	
	public Message(int senderId, int receiverId, String message) {
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.message = message;
	}
	public Message() {
		// TODO Auto-generated constructor stub
	}
	public int getSenderId() {
		return senderId;
	}
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	public int getReceiverId() {
		return receiverId;
	}
	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
