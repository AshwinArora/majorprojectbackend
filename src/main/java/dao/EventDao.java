package dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import model.Event;
import model.Interest;
import model.ResponseObject;
import model.User;
import helper.DatabaseHelper;
import interfaces.EventInterface;
import io.swagger.annotations.ApiParam;

public class EventDao implements EventInterface{

	@Override
	public String getAllEventByLocation(BigDecimal latitude,BigDecimal longitude,BigDecimal radius) {
		ResponseObject<ArrayList<Event>> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		ArrayList<Event> eventsList = new ArrayList<>();
		String sql = "CALL nearby_events(?,?,?)";
		JsonArray users = new JsonArray();
		try{
			conn = database.getConnection();
			CallableStatement callStmt = conn.prepareCall(sql);
			callStmt.setBigDecimal(1, latitude);
			callStmt.setBigDecimal(2, longitude);
			callStmt.setBigDecimal(3, radius);
			rs = callStmt.executeQuery();
			while(rs.next()){
				Event event = new Event();
				event.setId(rs.getInt("event_id"));
				event.setDescription(rs.getString("event_description"));
				event.setLatitude(rs.getBigDecimal("event_latitude"));
				event.setLongitude(rs.getBigDecimal("event_longitude"));
				event.setDate(rs.getLong("event_date"));
				event.setName(rs.getString("event_name"));
				event.setUser_id(rs.getInt("event_user_id"));
				event.setNumberOfUsersAttending(rs.getInt("event_numusers"));
				event.getType().setInterest_id(rs.getInt("event_interest_id"));
				event.getType().setCategory_id(rs.getInt("event_category_id"));
				eventsList.add(event);
			}
			response.setData(eventsList);
		}catch(Exception e){ 
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);

	}
	
	

	@Override
	public String getEvent(int event_id) {
		ResponseObject<Event> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		Event event = new Event();
		DatabaseHelper database = new DatabaseHelper();
		String getEvent = "select event_id,event_name,event_description,event_date,event_latitude,event_longitude,event_numusers,"
				+ "event_user_id,interest.interest_id,interest.interest_name,category.category_id,category.category_name "
				+ "from event,interest,category where interest.category_id=category.category_id and interest.interest_id=event.event_interest_id "
				+ "and category.category_id=event.event_category_id and event_id=? and active=?;";
		try{
			conn = database.getConnection();
			pstmt = conn.prepareStatement(getEvent);
			pstmt.setInt(1, event_id);
			pstmt.setInt(2, 1);
			System.out.println("getEvent Stmt - > "+pstmt);
			rs = pstmt.executeQuery();
			if(rs.next()){
				event.setId(rs.getInt("event_id"));
				event.setDescription(rs.getString("event_description"));
				event.setLatitude(rs.getBigDecimal("event_latitude"));
				event.setLongitude(rs.getBigDecimal("event_longitude"));
				event.setDate(rs.getLong("event_date"));
				event.setName(rs.getString("event_name"));
				event.setUser_id(rs.getInt("event_user_id"));
				event.setNumberOfUsersAttending(rs.getInt("event_numusers"));
				event.getType().setInterest_id(rs.getInt("interest_id"));
				event.getType().setCategory_id(rs.getInt("category_id"));
				event.getType().setCategory_name(rs.getString("category_name"));
				event.getType().setInterest_name(rs.getString("interest_name"));
			}
			String getUsersForEvent = "select * from user where user_id in (select user_id from event_users where event_id=? and active=?)";
			pstmt = conn.prepareStatement(getUsersForEvent);
			pstmt.setInt(1, event_id);
			pstmt.setInt(2, 1);
			rs = pstmt.executeQuery();
			while(rs.next()){
				User user = new User();
				user.setId(rs.getInt("user_id"));
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				user.setLatitude(rs.getBigDecimal("user_latitude"));
				user.setLongitude(rs.getBigDecimal("user_longitude"));
				/*String getInterestSql = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(getInterestSql);
				pstmt.setInt(1, user.getId());
				ResultSet rs1 = pstmt.executeQuery();
				while(rs1.next()){
					Interest interest = new Interest();
					interest.setInterest_id(rs1.getInt("interest_id"));
					interest.setInterest_name(rs1.getString("interest_name"));
					interest.setCategory_id(rs1.getInt("category_id"));
					interest.setCategory_name(rs1.getString("category_name"));
					System.out.println("ID : "+interest.getInterest_id());
					user.getInterests().add(interest);
				}*/
				event.getUsersAttending().add(user);
				//rs1.close();
			}
			response.setData(event);
		}catch(Exception e){ 
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return new Gson().toJson(response);
	}

	@Override
	public String getEventByUserId(int id) {
		ResponseObject<ArrayList<Event>> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		ArrayList<Event> eventsList = new ArrayList<>();
		DatabaseHelper database = new DatabaseHelper();
		try{
			conn = database.getConnection();
			String getEvents = "select ev.event_id,event_description,event_name,event_latitude,event_longitude,"
					+ "event_numusers,event_interest_id,event_category_id,event_date,event_user_id "
					+ "from event_users eu,event ev where user_id=? and eu.active=? and eu.active=ev.active and ev.event_id=eu.event_id";
			pstmt = conn.prepareStatement(getEvents);
			pstmt.setInt(1, id);
			pstmt.setInt(2, 1);
			rs = pstmt.executeQuery();
			ResultSet rs1 = null;
			while(rs.next()){
				Event event = new Event();
				event.setId(rs.getInt("event_id"));
				event.setDescription(rs.getString("event_description"));
				event.setLatitude(rs.getBigDecimal("event_latitude"));
				event.setLongitude(rs.getBigDecimal("event_longitude"));
				event.setDate(rs.getLong("event_date"));
				event.setName(rs.getString("event_name"));
				event.setUser_id(rs.getInt("event_user_id"));
				event.setNumberOfUsersAttending(rs.getInt("event_numusers"));
				event.getType().setInterest_id(rs.getInt("event_interest_id"));
				event.getType().setCategory_id(rs.getInt("event_category_id"));
				String sql = "select interest_name,category_name from interest,category where interest_id=? and interest.category_id=category.category_id";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, event.getType().getInterest_id());
				rs1 = pstmt.executeQuery();
				rs1.next();
				event.getType().setInterest_name(rs1.getString(1));
				event.getType().setCategory_name(rs1.getString(2));
				eventsList.add(event);
			}
			rs1.close();
			response.setData(eventsList);
		}catch(Exception e){ 
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return new Gson().toJson(response);
	}

	public String createEvent(Event event) {
		ResponseObject<Event> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		try{
			conn = database.getConnection();
			String getEvents = "INSERT INTO `interest_matcher`.`event` (`event_name`, `event_description`, "
					+ "`event_date`, `event_latitude`, `event_longitude`, `event_numusers`, `event_interest_id`, "
					+ "`event_category_id`, `event_user_id`, `created_on`, `last_updated`, `active`) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
			pstmt = conn.prepareStatement(getEvents,Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, event.getName());
			pstmt.setString(2,event.getDescription()); 
			pstmt.setLong(3, event.getDate());
			pstmt.setBigDecimal(4, event.getLatitude());
			pstmt.setBigDecimal(5, event.getLongitude());
			pstmt.setInt(6, 0);
			pstmt.setInt(7, event.getType().getInterest_id());
			pstmt.setInt(8, event.getType().getCategory_id());
			pstmt.setInt(9, event.getUser_id());
			pstmt.setLong(10, System.currentTimeMillis());
			pstmt.setLong(11, System.currentTimeMillis());
			pstmt.setInt(12, 1);
			int x = pstmt.executeUpdate();
			if(x<0)
				throw new Exception("Could not create event");
			rs = pstmt.getGeneratedKeys();
			if(!rs.next())
				throw new Exception("No id");
			event.setId(rs.getInt(1));
			String s = subscribeToEvent(event.getUser_id(), event.getId());
			response.setData(event);
		}catch(Exception e){ 
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return new Gson().toJson(response);
	}
	
	public String deleteEvent(int event_id) {
		ResponseObject<String> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		try{
			String delete = "update event set active=? where event_id = ? and active=?";
			pstmt.setInt(1, 0);
			pstmt.setInt(2, event_id);
			pstmt.setInt(3, 1);
			int x = pstmt.executeUpdate();
			if(x<0)
				throw new Exception("Could not delete");
		}catch(Exception e){ 
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return new Gson().toJson(response);
	}
	
	public String updateEvent(Event eventRec) {
		ResponseObject<Event> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		try{
			conn = database.getConnection();
			String updateSql = "update event set event_name=?,event_description=?,"
					+ "event_date=?,event_latitude=?,event_longitude=? "
					+ "where event_id=? and active=?";
			pstmt = conn.prepareStatement(updateSql);
			pstmt.setString(1, eventRec.getName());
			pstmt.setString(2, eventRec.getDescription());
			pstmt.setLong(3, eventRec.getDate());
			pstmt.setBigDecimal(4, eventRec.getLatitude());
			pstmt.setBigDecimal(5, eventRec.getLongitude());
			pstmt.setInt(6, eventRec.getId());
			pstmt.setInt(7,1);
			int x = pstmt.executeUpdate();
			String getEvents = "select ev.event_id,event_description,event_name,event_latitude,event_longitude,"
					+ "event_numusers,event_interest_id,event_category_id,event_date,event_user_id "
					+ "from event_users eu,event ev where user_id=? and eu.active=? and eu.active=ev.active and ev.event_id=eu.event_id";
			pstmt = conn.prepareStatement(getEvents);
			pstmt.setInt(1, eventRec.getId());
			pstmt.setInt(2, 1);
			rs = pstmt.executeQuery();
			ResultSet rs1 = null;
			Event event = new Event();
			while(rs.next()){
				event.setId(rs.getInt("event_id"));
				event.setDescription(rs.getString("event_description"));
				event.setLatitude(rs.getBigDecimal("event_latitude"));
				event.setLongitude(rs.getBigDecimal("event_longitude"));
				event.setDate(rs.getLong("event_date"));
				event.setName(rs.getString("event_name"));
				event.setUser_id(rs.getInt("event_user_id"));
				event.setNumberOfUsersAttending(rs.getInt("event_numusers"));
				event.getType().setInterest_id(rs.getInt("event_interest_id"));
				event.getType().setCategory_id(rs.getInt("event_category_id"));
				String sql = "select interest_name,category_name from interest,category where interest_id=? and interest.category_id=category.category_id";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, event.getType().getInterest_id());
				rs1 = pstmt.executeQuery();
				rs1.next();
				event.getType().setInterest_name(rs1.getString(1));
				event.getType().setCategory_name(rs1.getString(2));
			}
			response.setData(event);
		}catch(Exception e){ 
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return new Gson().toJson(response);
	}
	
	public String subscribeToEvent(int user_id,int event_id){
 		ResponseObject<String> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
			conn.setAutoCommit(false);
			String sqlCheck = "select * from event_users where event_id=? and user_id=? and active=?";
			pstmt = conn.prepareStatement(sqlCheck);
			pstmt.setInt(1, event_id);
			pstmt.setInt(2, user_id);
			pstmt.setInt(3, 1);
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("Already Subscribed");
			String sql = "insert into event_users(event_id,user_id,active)  values(?,?,?) "
					+ "on duplicate key update active=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, event_id);
			pstmt.setInt(2, user_id);
			pstmt.setInt(3, 1);
			pstmt.setInt(4, 1);
			int i = pstmt.executeUpdate();
			if(i<=0)
				throw new Exception("Could not Subscribe");
			String sql2 = "update event as ev set ev.event_numusers = "
					+ "ev.event_numusers+1 where ev.event_id = ? ";
			pstmt = conn.prepareStatement(sql2);
			pstmt.setInt(1, event_id);
			int k = pstmt.executeUpdate();
			if(k<=0)
				throw new Exception("Could not update event");
			conn.commit();
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);
 	}
	
	public String unsubscribeToEvent(int user_id,int event_id){
 		ResponseObject<String> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
			conn.setAutoCommit(false);
			String sql = "update event_users set active = ? where event_id=? and user_id=? and active=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, 0);
			pstmt.setInt(2, event_id);
			pstmt.setInt(3, user_id);
			pstmt.setInt(4, 1);
			int i = pstmt.executeUpdate();
			System.out.println("Unsub : "+i);
			if(i<=0)
				throw new Exception("Could not unSubscribe");
			String sql2 = "update event as ev set ev.event_numusers = "
					+ "ev.event_numusers-1 where ev.event_id = ? ";
			pstmt = conn.prepareStatement(sql2);
			pstmt.setInt(1, event_id);
			int k = pstmt.executeUpdate();
			if(k<=0)
				throw new Exception("Could not update event");
			conn.commit();
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);
 	}
	
}
