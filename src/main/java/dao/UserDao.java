package dao;

import helper.DatabaseHelper;
import interfaces.UserInterface;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import model.Category;
import model.Interest;
import model.Login;
import model.Recommended;
import model.ResponseObject;
import model.User;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.simmachines.libsim.enc.binary.Dice;

public class UserDao implements UserInterface{

	public String createUser(User user){
		ResponseObject<User> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		
		try{
			String name = user.getName();
			String number = user.getNumber();
			String emailId = user.getEmail_id();
			String password = user.getPassword();
			conn= database.getConnection();
			String checkEmail = "select u.user_id from user u where u.user_email_id = ?";
			pstmt = conn.prepareStatement(checkEmail);
			pstmt.setString(1, emailId);
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("User email already exists");
			String checkNumber = "select u.user_id from user u where u.user_number = ?";
			pstmt = conn.prepareStatement(checkNumber);
			pstmt.setString(1, number);
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("User number already exists");
			String insert = "insert into user(user_name,user_email_id,user_number,user_password,user_latitude,user_longitude,created_on,last_updated,active) "
					+ "values(?,?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(insert,Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, name);
			pstmt.setString(2, emailId);
			pstmt.setString(3, number);
			pstmt.setString(4,password);
			pstmt.setBigDecimal(5, user.getLatitude());
			pstmt.setBigDecimal(6, user.getLongitude());
			pstmt.setLong(7,System.currentTimeMillis());
			pstmt.setLong(8,System.currentTimeMillis());
			pstmt.setInt(9, 1);
			int row = pstmt.executeUpdate();
			if(row<=0)
				throw new Exception("Some error occured.Contact Admin");
			rs = pstmt.getGeneratedKeys();
			int id =-1;
			if(rs.next()){
				id = rs.getInt(1);
				user.setId(id);
			}
			else
				throw new Exception("Some error occured");
			response.setData(user);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage()+"E");
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}

	public String userLogin(Login userR){
		ResponseObject<User> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null; 
		User user = new User();
		DatabaseHelper database = new DatabaseHelper();
		
		try{
			String emailId = userR.getEmail_id();
			String password = userR.getPassword();
			conn= database.getConnection();
			String sql = "select * from user u where u.user_email_id = ? and u.user_password =? and u.active = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, emailId);
			pstmt.setString(2, password);
			pstmt.setInt(3, 1);
			rs = pstmt.executeQuery();
			if(rs.next()){
				int userId = rs.getInt("user_id");
				user.setId(userId);
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				sql = "select * from user_interest_view uiv where uiv.user_id = ? and uiv.active = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, userId);
				pstmt.setInt(2, 1);
				rs = pstmt.executeQuery();
				while(rs.next()){
					Interest interest = new Interest();
					interest.setInterest_id(rs.getInt("interest_id"));
					interest.setInterest_name(rs.getString("interest_name"));
					interest.setCategory_id(rs.getInt("category_id"));
					interest.setCategory_name(rs.getString("category_name"));
					user.getInterests().add(interest);
				}
				response.setData(user);
			}else
				throw new Exception("Username or password is incorrect or does not exist");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}

	public String getUser(User userReceived){
		ResponseObject<User> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		User user = new User();
		int userId = userReceived.getId();
		DatabaseHelper database = new DatabaseHelper();
		try{
			conn= database.getConnection();
			String sql = "select user_name,user_number,user_email_id,user_latitude,user_longitude from user u where u.user_id = ? and u.active = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, 1);
			rs = pstmt.executeQuery();
			if(rs.next()){
				user.setId(userId);
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				user.setLatitude(rs.getBigDecimal("user_latitude"));
				user.setLongitude(rs.getBigDecimal("user_longitude"));
				sql = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, userId);
				rs = pstmt.executeQuery();
				while(rs.next()){
					Interest interest = new Interest();
					interest.setInterest_id(rs.getInt("interest_id"));
					interest.setInterest_name(rs.getString("interest_name"));
					interest.setCategory_id(rs.getInt("category_id"));
					interest.setCategory_name(rs.getString("category_name"));
					System.out.println("ID : "+interest.getInterest_id());
					user.getInterests().add(interest);
				}
				response.setData(user);
			}else
				throw new Exception("User not active or doesnt exist");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println(new Gson().toJson(response));
		return new Gson().toJson(response);
	}

	public String updateUser(User user){
		ResponseObject<User> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		try{
			conn= database.getConnection();
			conn.setAutoCommit(false);
			String checkEmail = "Select u1.user_id from user u1 where u1.user_id <> ? and u1.user_email_id = ?";
			pstmt = conn.prepareStatement(checkEmail);
			pstmt.setInt(1, user.getId());
			pstmt.setString(2, user.getEmail_id());
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("User email already exists");
			String checkNum = "Select u1.user_id from user u1 where u1.user_id <> ? and u1.user_number = ?" ;
			pstmt = conn.prepareStatement(checkNum);
			pstmt.setInt(1, user.getId());
			pstmt.setString(2, user.getNumber());
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("User number already exists");
			Type listType = new TypeToken<ArrayList<Interest>>(){}.getType();
			ArrayList<Interest> interestsNew = user.getInterests();
			String updateUser = "update user u set u.user_name = ? , u.user_email_id = ?"
					+ ", u.user_number = ?, u.user_password = ?,"
					+ " u.last_updated = ?  where u.user_id = ? and u.active = ?";
			pstmt = conn.prepareStatement(updateUser);
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getEmail_id());
			pstmt.setString(3, user.getNumber());
			pstmt.setString(4,user.getPassword());
			pstmt.setLong(5,System.currentTimeMillis());
			pstmt.setInt(6, user.getId());
			pstmt.setInt(7, 1);
			int row = pstmt.executeUpdate();
			if(row<=0)
				throw new Exception("Could not update user");
			String inactiveInterest = "update user_interests ui set ui.active=? where ui.user_id=?";
			pstmt = conn.prepareStatement(inactiveInterest);
			pstmt.setInt(1, 0);
			pstmt.setInt(2, user.getId());
			int r = pstmt.executeUpdate();
			String insertInterests = "insert into user_interest(user_id,interest_id,created_on,last_updated,active) values(?,?,?,?,?)"
					+"on duplicate key update active = values(active), last_updated=values(last_updated)";
			pstmt = conn.prepareStatement(insertInterests);
			for(Interest interest : interestsNew){
				pstmt.setInt(1, user.getId());
				pstmt.setInt(2, interest.getInterest_id());
				pstmt.setLong(3, System.currentTimeMillis());
				pstmt.setLong(4, System.currentTimeMillis());
				pstmt.setInt(5, 1);
				pstmt.addBatch();
			}
			int res[] = pstmt.executeBatch();
			for(int i : res)
				if(i<0)
					throw new Exception("Could not update Interests");
			String select = "select user_id,user_name,user_number,user_email_id,user_latitude,user_longitude from user u where u.user_id = ? and u.active = ?";
			pstmt = conn.prepareStatement(select);
			pstmt.setInt(1, user.getId());
			pstmt.setInt(2, 1);
			rs = pstmt.executeQuery();
			if(rs.next()){
				user.setId(rs.getInt("user_id"));
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				user.setLatitude(rs.getBigDecimal("user_latitude"));
				user.setLongitude(rs.getBigDecimal("user_longitude"));
				select = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(select);
				pstmt.setInt(1, user.getId());
				rs = pstmt.executeQuery();

				while(rs.next()){
					Interest interest = new Interest();
					interest.setInterest_id(rs.getInt("interest_id"));
					interest.setInterest_name(rs.getString("interest_name"));
					interest.setCategory_id(rs.getInt("category_id"));
					interest.setCategory_name(rs.getString("category_name"));
					user.getInterests().add(interest);
				}
				response.setData(user);
			}
			conn.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			try { if (rs != null) conn.rollback(); } catch (SQLException ef) {
				ef.printStackTrace();
			}
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}

	public String getNearbyUsers(BigDecimal latitude,BigDecimal longitude,BigDecimal radius,int user_id){
		ResponseObject<JsonArray> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		DatabaseHelper database = new DatabaseHelper();
		String sql = "CALL nearby_users(?,?,?)";
		JsonArray users = new JsonArray();
		try{
			conn = database.getConnection();
			CallableStatement callStmt = conn.prepareCall(sql);
			callStmt.setBigDecimal(1, latitude);
			callStmt.setBigDecimal(2, longitude);
			callStmt.setBigDecimal(3, radius);
			rs = callStmt.executeQuery();
			Map<Integer,JsonObject> m = new LinkedHashMap<>();
			while(rs.next()){
				JsonObject user = new JsonObject();
				int id = rs.getInt(1);
				if(id == user_id)
					continue;
				user.addProperty("id",id);
				user.addProperty("name",rs.getString(2));
				user.addProperty("email_id",rs.getString(3));
				user.addProperty("number",rs.getString(4));
				user.addProperty("latitude",rs.getBigDecimal(5));
				user.addProperty("longitude",rs.getBigDecimal(6));
				user.addProperty("distance",rs.getInt(7));
				sql = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, id);
				rs1 = pstmt.executeQuery();
				JsonArray interests = new JsonArray();
				while(rs1.next()){
					Interest interest = new Interest();
					interest.setInterest_id(rs1.getInt("interest_id"));
					interest.setInterest_name(rs1.getString("interest_name"));
					interest.setCategory_id(rs1.getInt("category_id"));
					interest.setCategory_name(rs1.getString("category_name"));
					interests.add(new Gson().toJsonTree(interest));
				}
				user.add("interests", interests);
				m.put(id, user);
			}
			String s = "select * from user_matches where user_id=?";
			pstmt = conn.prepareStatement(s);
			pstmt.setInt(1, user_id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				m.remove(rs.getInt("user_matched_id"));
			}
			//users.add();
			int limit = 20,i=0;
			for(JsonObject x: m.values()){
				if(i>=limit)
					break;
				users.add(x);
				i++;
			}
			System.out.println("users - "+new Gson().toJson(users));
			response.setData(users);
		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close();rs1.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Response - "+new Gson().toJson(response));
		return new Gson().toJson(response);
	}

	public String getRecommendedUsers(BigDecimal latitude,BigDecimal longitude,BigDecimal radius,int user_id){
		ResponseObject<JsonArray> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		DatabaseHelper database = new DatabaseHelper();
		//JsonObject receivedData = new JsonParser().parse(data;
		int limit = 100;
		String sql = "CALL nearby_users(?,?,?)";
		JsonArray users = new JsonArray();
		try{
			conn = database.getConnection();
			ArrayList<Integer> list = new ArrayList<>();
			String s = "select * from user_matches where user_id=?";
			pstmt = conn.prepareStatement(s);
			pstmt.setInt(1, user_id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				list.add(rs.getInt("user_matched_id"));
			}
			CallableStatement callStmt = conn.prepareCall(sql);
			callStmt.setBigDecimal(1, latitude);
			callStmt.setBigDecimal(2, longitude);
			callStmt.setBigDecimal(3, radius);
			rs = callStmt.executeQuery();
			Map<Integer,JsonObject> m = new LinkedHashMap<>();
			ArrayList<Recommended> recUsersList = new ArrayList<>();
			while(rs.next()){
				int id = rs.getInt(1);
				if(id == user_id)
					continue;
				if(list.contains(id))
					continue;
				JsonObject user = new JsonObject();
				user.addProperty("id",id);
				user.addProperty("name",rs.getString(2));
				user.addProperty("email_id",rs.getString(3));
				user.addProperty("number",rs.getString(4));
				user.addProperty("latitude",rs.getBigDecimal(5));
				user.addProperty("longitude",rs.getBigDecimal(6));
				user.addProperty("distance",rs.getInt(7));
				sql = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, id);
				rs1 = pstmt.executeQuery();
				JsonArray interests = new JsonArray();
				while(rs1.next()){
					Interest interest = new Interest();
					interest.setInterest_id(rs1.getInt("interest_id"));
					interest.setInterest_name(rs1.getString("interest_name"));
					interest.setCategory_id(rs1.getInt("category_id"));
					interest.setCategory_name(rs1.getString("category_name"));
					interests.add(new Gson().toJsonTree(interest));
				}
				user.add("interests", interests);
				Recommended rectemp = new Recommended();
				rectemp.setUser_id(id);
				for(int k=0;k<interests.size();k++){
					JsonObject i = interests.get(k).getAsJsonObject();
					int intId = i.get("interest_id").getAsInt();
					if(intId>=13)
						intId -= 9; 
					rectemp.addInterestPresent(intId-1);
				}
				recUsersList.add(rectemp);
				m.put(id, user);
			}
			String curUserSql = "select * from user_interest_view uiv where uiv.user_id = ? ";
			pstmt = conn.prepareStatement(curUserSql);
			pstmt.setInt(1, user_id);
			System.out.println("Stmt : " +pstmt + user_id);
			rs = pstmt.executeQuery();
			Recommended currUser = new Recommended();
			currUser.setUser_id(user_id);
			currUser.setSimilarity(1);
			while(rs.next()){
				int intId = rs.getInt("interest_id");
				System.out.println("Interest Id : "+intId);
				if(intId>=13)
					intId -= 9; 
				currUser.addInterestPresent(intId-1);
			}
			for(int tempIndex = 0;tempIndex<recUsersList.size();tempIndex++){
				Recommended temp = recUsersList.get(tempIndex);
				System.out.println("User Id : "+temp.getUser_id());
				temp.setSimilarity(1 - Dice.distance(temp.getUser_interests(),currUser.getUser_interests()));				
			}
			Collections.sort(recUsersList);
			Collections.reverse(recUsersList);
			
			//users.add();
			int i=0;
			for(Recommended x: recUsersList){
				if(i>=limit)
					break;
				m.get(x.getUser_id()).addProperty("similarity", x.getSimilarity());
				users.add(m.get(x.getUser_id()));
				i++;
			}
			System.out.println("users - "+new Gson().toJson(users));
			response.setData(users);
		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}

	@SuppressWarnings("unchecked")
	public String addInterests(User user){
		ResponseObject<String> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();		
		try{
			int user_id = user.getId();
			String insertInterests = "insert into user_interests(user_id,interest_id,created_on,last_updated,active) values(?,?,?,?,?)";
			ArrayList<Interest> addInterests = user.getInterests();
			conn = database.getConnection();
			pstmt = conn.prepareStatement(insertInterests);
			for(Interest interest : addInterests){
				pstmt.setInt(1, user_id);
				pstmt.setInt(2, interest.getInterest_id());
				pstmt.setLong(3, System.currentTimeMillis());
				pstmt.setLong(4, System.currentTimeMillis());
				pstmt.setInt(5, 1);
				pstmt.addBatch();
			}
			int res[] = pstmt.executeBatch();
			for(int i : res)
				if(i<0)
					throw new Exception("Could not add Interests");
		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}

	/*public String getInterests(){
		ResponseObject<Category> response = new ResponseObject<>();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();		 		
		try{
			conn = database.getConnection();
			List<Category> categories = new ArrayList<>();
			List<Interest> interests = new ArrayList<>();
			String getCategories = "select * from category";
			String getInterests = "select * from interest";
			Statement stmt = conn.createStatement();
			stmt.execute(getCategories);
			rs = stmt.getResultSet();
			while(rs.next()){
				categories.add(new Category(rs.getInt("category_id"),
						rs.getString("category_name")));
			}
			stmt.execute(getInterests);
			rs = stmt.getResultSet();
			while(rs.next()){
				interests.add(new Interest(rs.getInt("interest_id"),
						rs.getString("interest_name"),
						rs.getInt("category_id"),
						rs.getString("category_name")));
			}
			JsonObject data = new JsonObject();
			data.add("category", categories,new TypeToken<List<Category>>(){}.getType()));
			data.add("interest", interests,new TypeToken<List<Interest>>(){}.getType()));
			response.setData(data);
			
		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return new Gson().toJson(response);
	*/
	
	public String getCategoryAndInterests(){
 		ResponseObject<ArrayList<Category>> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		ArrayList<Category> categories = new ArrayList<>();
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
			String sql = "select * from category";
			Statement stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				Category c = new Category();
				c.setCategory_id(rs.getInt("category_id"));
				c.setCategory_name(rs.getString("category_name"));
				String in = "select * from interest where category_id=?";
				pstmt = conn.prepareStatement(in);
				pstmt.setInt(1, c.getCategory_id());
				ResultSet rs1 = pstmt.executeQuery();
				while(rs1.next()){
					Interest i = new Interest();
					i.setInterest_id(rs1.getInt("interest_id"));
					i.setInterest_name(rs1.getString("interest_name"));
					i.setCategory_id(c.getCategory_id());
					i.setCategory_name(c.getCategory_name());
					c.getInterests().add(i);
				}
				rs1.close();
				categories.add(c);
			}
			System.out.println(new Gson().toJson(categories));
			response.setData(categories);
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);
 	}
	
	public String userLikes(int userId,int userLikedId){
		ResponseObject<JsonObject> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		JsonObject isMatch = new JsonObject();
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
			String addLike = "insert into user_likes(user_id,user_liked_id,"
					+ "created_on,last_updated) values(?,?,?,?)";
			pstmt = conn.prepareStatement(addLike);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, userLikedId);
			pstmt.setLong(3, System.currentTimeMillis());
			pstmt.setLong(4, System.currentTimeMillis());
			int x = pstmt.executeUpdate();
			if(x<0)
				throw new Exception("Could not insert");
			String isMatchSql = "select user_id from user_likes where user_id=?"
					+ " and user_liked_id=?";
			pstmt = conn.prepareStatement(isMatchSql);
			pstmt.setInt(1, userLikedId);
			pstmt.setInt(2, userId);
			rs = pstmt.executeQuery();
			if(rs.next()){
				isMatch.addProperty("isMatch", true);
				String addMatch = "insert into user_matches(user_id,user_matched_id,created_on) "
						+ "values(?,?,?)";
				pstmt = conn.prepareStatement(addMatch);
				pstmt.setInt(1, userId);
				pstmt.setInt(2, userLikedId);
				pstmt.setLong(3, System.currentTimeMillis());
				int y = pstmt.executeUpdate();
				if(y<0)
					throw new Exception("Could not insert Match");
			}else
				isMatch.addProperty("isMatch", false);
			response.setData(isMatch);
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);
	}
	
	public String getMatchedUserList(int id){
		ResponseObject<ArrayList<User>> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		ArrayList<User> users = new ArrayList<>();
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
			String getMatchedUsers = "select * from user where user_id in "
					+ "(select user_matched_id from user_matches where user_id=?  ) OR user_id in (select user_id from user_matches where user_matched_id=?) and active=?";
			pstmt = conn.prepareStatement(getMatchedUsers);
			pstmt.setInt(1, id);
			pstmt.setInt(2, id);
			pstmt.setInt(3, 1);
			System.out.println("Stmt - "+pstmt);
			rs = pstmt.executeQuery();
			while(rs.next()){
				User user = new User();
				user.setId(rs.getInt("user_id"));
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				user.setLatitude(rs.getBigDecimal("user_latitude"));
				user.setLongitude(rs.getBigDecimal("user_longitude"));
				/*String getInterestSql = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(getInterestSql);
				pstmt.setInt(1, user.getId());
				ResultSet rs1 = pstmt.executeQuery();
				while(rs1.next()){
					Interest interest = new Interest();
					interest.setInterest_id(rs1.getInt("interest_id"));
					interest.setInterest_name(rs1.getString("interest_name"));
					interest.setCategory_id(rs1.getInt("category_id"));
					interest.setCategory_name(rs1.getString("category_name"));
					System.out.println("ID : "+interest.getInterest_id());
					user.getInterests().add(interest);
				}*/
				//rs1.close();
				users.add(user);
			}
			response.setData(users);
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);

	}
	
	public String updateLocation(BigDecimal latitude,BigDecimal longitude,int user_id){
 		ResponseObject<User> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
			String updateSQL = "UPDATE user set user_latitude=?"
					+ ",user_longitude=? where user_id=? "
					+ "and active=?";
			pstmt = conn.prepareStatement(updateSQL);
			pstmt.setBigDecimal(1, latitude);
			pstmt.setBigDecimal(2, longitude);
			pstmt.setInt(3, user_id);
			pstmt.setInt(4, 1);
			int x = pstmt.executeUpdate();
			if(x<0)
				throw new Exception("Cant update location");
			String sql = "select user_name,user_number,user_email_id,user_latitude,user_longitude from user u where u.user_id = ? and u.active = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, user_id);
			pstmt.setInt(2, 1);
			rs = pstmt.executeQuery();
			User user = new User();
			if(rs.next()){
				user.setId(user_id);
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				user.setLatitude(rs.getBigDecimal("user_latitude"));
				user.setLongitude(rs.getBigDecimal("user_longitude"));
				sql = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, user_id);
				rs = pstmt.executeQuery();
				while(rs.next()){
					Interest interest = new Interest();
					interest.setInterest_id(rs.getInt("interest_id"));
					interest.setInterest_name(rs.getString("interest_name"));
					interest.setCategory_id(rs.getInt("category_id"));
					interest.setCategory_name(rs.getString("category_name"));
					System.out.println("ID : "+interest.getInterest_id());
					user.getInterests().add(interest);
				}
			}
				response.setData(user);
			
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);
 	}
}

/***
 * 
 * Template for method
 * 
 * public String getNearbyUsers(String data){
 		ResponseObject<> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);
 	}
 */ 
