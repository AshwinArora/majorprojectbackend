package service;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import model.Event;
import model.ResponseObject;
import dao.EventDao;
import interfaces.EventInterface;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("/EventService")
@Api(value="/EventService")
public class EventService implements EventInterface{ 

	@Path("/location")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="Return events happening in the radius specified by user.Working now",response=Event.class,responseContainer="List")
	public String getAllEventByLocation(@ApiParam(value="latitude of user") @QueryParam("latitude")BigDecimal latitude,
			@ApiParam(value="longitude of user")@QueryParam("longitude")BigDecimal longitude,
			@ApiParam(value="radius provided by user") @QueryParam("radius") BigDecimal radius) {
		EventDao dao = new EventDao();
		return dao.getAllEventByLocation(latitude,longitude,radius);
	}

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="Returns event details by its id",response=Event.class)
	public String getEvent(@ApiParam(value="Id of event")@PathParam("id") int event_id) {
		EventDao dao = new EventDao();
		return dao.getEvent(event_id);
	}

	@Path("/user/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="Returns All events of user created or participated in",response=Event.class,responseContainer="List")
	public String getEventByUserId(@ApiParam(value="Id of user")@PathParam("id")int user_id) {
		EventDao dao = new EventDao();
		return dao.getEventByUserId(user_id);
	}
	
	@Path("/create")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value="Creates an event",notes="Consumes and responds with event object ",response=Event.class)
	public String createEvent(@ApiParam(value=" ") Event event_data) {
		EventDao dao = new EventDao();
		return dao.createEvent(event_data);
	}
	
	@Path("/delete")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value="delete an event",notes="Consumes and responds with event object ",response=Event.class)
	public String deleteEvent(@ApiParam(value="Consumes and responds with event object - ") int event_id) {
		EventDao dao = new EventDao();
		return dao.deleteEvent(event_id);
	}
	
	@Path("/update")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value="update an event. Working Now",notes="Consumes and responds with event object ",response=Event.class)
	public String updateEvent(@ApiParam(value="Send everything whether its changed or not") Event event_data) {
		EventDao dao = new EventDao();
		return dao.updateEvent(event_data);
	}
	
	@Path("/{event_id}/subscribe/{user_id}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="allow user who is attending the event to subscribe to it",notes="responds with success = true if database operations are successful",response = ResponseObject.class)
	public String subscribeToEvent(@ApiParam(value="Id of user")@PathParam("user_id")int user_id,@ApiParam(value="Id of event to be subscribed to or attending")@PathParam("event_id")int event_id) {
		EventDao dao = new EventDao();
		return dao.subscribeToEvent(user_id, event_id);
	}
	
	@Path("/{event_id}/unsubscribe/{user_id}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="allow user who is attending the event to unsubscribe to it(not attending now)",notes="responds with success = true if database operations are successful",response = ResponseObject.class)
	public String unsubscribeToEvent(@ApiParam(value="Id of user")@PathParam("user_id")int user_id,@ApiParam(value="Id of event to be unsubscribed to or attending")@PathParam("event_id")int event_id) {
		EventDao dao = new EventDao();
		System.out.println("Unsub = dsfdsfdsfsdfds");
		return dao.unsubscribeToEvent(user_id, event_id);
	}

}
