package model;

public class Interest {
	int interest_id;
	String interest_name;
	int category_id;
	String category_name;
	
	
	
	public Interest(int interest_id, String interest_name, int category_id,
			String category_name) {
		this.interest_id = interest_id;
		this.interest_name = interest_name;
		this.category_id = category_id;
		this.category_name = category_name;
	}
	public Interest() {
		// TODO Auto-generated constructor stub
	}
	public int getInterest_id() {
		return interest_id;
	}
	public void setInterest_id(int interest_id) {
		this.interest_id = interest_id;
	}
	public String getInterest_name() {
		return interest_name;
	}
	public void setInterest_name(String interest_name) {
		this.interest_name = interest_name;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

}
