package interfaces;

import model.Message;

public interface MessageInterface {
	public String sendMessage(Message message);
	public String getAllMessages(int sender_id,int receiver_id);
	//public String getMessages(long timestamp,Message message);
}
