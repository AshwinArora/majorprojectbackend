package service;

import interfaces.UserInterface;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import dao.UserDao;

@Path("/UserService")
public class UserService implements UserInterface{
	
	@Path("/userSignUp")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String createUser(String data){
		UserDao dao = new UserDao();
		return dao.createUser(data);
	}
	
	@Path("/userLogin")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String userLogin(String data){
		UserDao dao = new UserDao();
		return dao.userLogin(data);
	}
	
	@Path("/getUser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String getUser(String data){
		UserDao dao = new UserDao();
		return dao.getUser(data);
	}
	
	@Path("/editUser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateUser(String data){
		UserDao dao = new UserDao();
		return dao.updateUser(data);
	}

	@Path("/addInterests")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addInterests(String data) {
		UserDao dao = new UserDao();
		return dao.addInterests(data);
	}

	@Path("/getNearbyUsers")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String getNearbyUsers(String data) {
		UserDao dao = new UserDao();
		return dao.getNearbyUsers(data);
	}

	@Path("/getRecommendedUsers")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String getRecommendedUsers(String data) {
		UserDao dao = new UserDao();
		return dao.getRecommendedUsers(data);
	}
	

}
