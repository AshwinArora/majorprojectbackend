package dao;

import model.Message;
import helper.DatabaseHelper;
import interfaces.MessageInterface;
import model.Category;
import model.Interest;
import model.Login;
import model.ResponseObject;
import model.User;

import com.google.gson.Gson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MessageDao implements MessageInterface {

	@Override
	public String sendMessage(Message message) {
		ResponseObject<String> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
			String sql = "insert into messages(sender_id,receiver_id,message,"
					+ "created_on) values(?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, message.getSenderId());
			pstmt.setInt(2, message.getReceiverId());
			pstmt.setString(3, message.getMessage());
			pstmt.setLong(4, System.currentTimeMillis());
			int x = pstmt.executeUpdate();
			if(x<0)
				throw new Exception("Could not insert message");
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);
	}

	@Override
	public String getAllMessages(int sender_id,int receiver_id) {
		ResponseObject<ArrayList<Message>> response = new ResponseObject<>();
 		PreparedStatement pstmt = null;
 		Connection conn = null;
 		ResultSet rs = null;
 		ArrayList<Message> messages = new ArrayList<>();
 		DatabaseHelper database = new DatabaseHelper();
 		try{
			conn = database.getConnection();
			String sql = "select * from messages where "
					+ "(sender_id = ? and receiver_id=?) or "
					+ "(sender_id = ? and receiver_id=?) order by created_on";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, sender_id);
			pstmt.setInt(2, receiver_id);
			pstmt.setInt(3, receiver_id);
			pstmt.setInt(4, sender_id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				Message m = new Message();
				m.setSenderId(rs.getInt("sender_id"));
				m.setReceiverId(rs.getInt("receiver_id"));
				m.setMessage(rs.getString("message"));
				messages.add(m);
			}
			response.setData(messages);
 		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
 		return new Gson().toJson(response);
	}

}
