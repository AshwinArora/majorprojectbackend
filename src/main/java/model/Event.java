package model;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Event {
	int id;
	String name;
	String description;
	BigDecimal latitude;
	BigDecimal longitude;
	int user_id;
	int numberOfUsersAttending;
	ArrayList<User> usersAttending;
	Interest type;
	long date;
	public Event(){
		this.usersAttending = new ArrayList<User>();
		this.type = new Interest();
	}
	public Event(int id, String name, String description, BigDecimal latitude,
			BigDecimal longitude, int user_id, int numberOfUsersAttending,
			ArrayList<User> usersAttending, Interest type, long date) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
		this.user_id = user_id;
		this.numberOfUsersAttending = numberOfUsersAttending;
		this.usersAttending = usersAttending;
		this.type = type;
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getNumberOfUsersAttending() {
		return numberOfUsersAttending;
	}
	public void setNumberOfUsersAttending(int numberOfUsersAttending) {
		this.numberOfUsersAttending = numberOfUsersAttending;
	}
	public ArrayList<User> getUsersAttending() {
		return usersAttending;
	}
	public void setUsersAttending(ArrayList<User> usersAttending) {
		this.usersAttending = usersAttending;
	}
	public Interest getType() {
		return type;
	}
	public void setType(Interest type) {
		this.type = type;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}		
}
