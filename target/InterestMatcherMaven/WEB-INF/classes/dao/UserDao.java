package dao;

import helper.DatabaseHelper;
import interfaces.UserInterface;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Interest;
import model.ResponseObject;
import model.User;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class UserDao implements UserInterface{


	public String createUser(String data){
		ResponseObject response = new ResponseObject();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		User user = new User();
		DatabaseHelper database = new DatabaseHelper();
		JsonObject receivedData = new JsonParser().parse(data).getAsJsonObject();
		try{
			String name = receivedData.get("user_name").getAsString();
			user.setName(name);
			String number = receivedData.get("user_number").getAsString();
			user.setNumber(number);
			String emailId = receivedData.get("user_email_id").getAsString();
			user.setEmail_id(emailId);
			String password = receivedData.get("user_password").getAsString();
			user.setPassword(password);
			conn= database.getConnection();
			String checkEmail = "select u.user_id from user u where u.user_email_id = ?";
			pstmt = conn.prepareStatement(checkEmail);
			pstmt.setString(1, emailId);
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("User email already exists");
			String checkNumber = "select u.user_id from user u where u.user_number = ?";
			pstmt = conn.prepareStatement(checkNumber);
			pstmt.setString(1, number);
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("User number already exists");
			String insert = "insert into user(user_name,user_email_id,user_number,user_password,created_on,last_updated,active) "
					+ "values(?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(insert,Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, name);
			pstmt.setString(2, emailId);
			pstmt.setString(3, number);
			pstmt.setString(4,password);
			pstmt.setLong(5,System.currentTimeMillis());
			pstmt.setLong(6,System.currentTimeMillis());
			pstmt.setInt(7, 1);
			int row = pstmt.executeUpdate();
			if(row<=0)
				throw new Exception("Some error occured");
			rs = pstmt.getGeneratedKeys();
			int id =-1;
			if(rs.next()){
				id = rs.getInt(1);
				user.setId(id);
			}
			else
				throw new Exception("Some error occured");
			response.setData(new Gson().toJsonTree(user).getAsJsonObject());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}

	public String userLogin(String data){
		ResponseObject response = new ResponseObject();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null; 
		User user = new User();
		DatabaseHelper database = new DatabaseHelper();
		JsonObject receivedData = new JsonParser().parse(data).getAsJsonObject();
		try{
			String emailId = receivedData.get("user_email_id").getAsString();
			String password = receivedData.get("user_password").getAsString();
			conn= database.getConnection();
			String sql = "select * from user u where u.user_email_id = ? and u.user_password =? and u.active = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, emailId);
			pstmt.setString(2, password);
			pstmt.setInt(3, 1);
			rs = pstmt.executeQuery();
			if(rs.next()){
				int userId = rs.getInt("user_id");
				user.setId(userId);
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				sql = "select * from user_interest_view uiv where uiv.user_id = ? and uiv.active = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, userId);
				pstmt.setInt(2, 1);
				rs = pstmt.executeQuery();
				Interest interest = new Interest();
				while(rs.next()){
					interest.setInterest_id(rs.getInt("interest_id"));
					interest.setInterest_name(rs.getString("interest_name"));
					interest.setCategory_id(rs.getInt("category_id"));
					interest.setCategory_name(rs.getString("category_name"));
					user.getInterests().add(interest);
				}
				response.setData(new Gson().toJsonTree(user).getAsJsonObject());
			}else
				throw new Exception("Username or password is incorrect or does not exist");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}

	public String getUser(String data){
		ResponseObject response = new ResponseObject();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		User user = new User();
		DatabaseHelper database = new DatabaseHelper();
		JsonObject receivedData = new JsonParser().parse(data).getAsJsonObject();
		try{
			conn= database.getConnection();
			int userId = receivedData.get("user_id").getAsInt(); 
			String sql = "select user_name,user_number,user_email_id,user_latitude,user_longitude from user u where u.user_id = ? and u.active = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, 1);
			rs = pstmt.executeQuery();
			if(rs.next()){
				user.setId(userId);
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				user.setLatitude(rs.getBigDecimal("user_latitude"));
				user.setLongitude(rs.getBigDecimal("user_longitude"));
				sql = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, userId);
				rs = pstmt.executeQuery();
				Interest interest = new Interest();
				while(rs.next()){
					interest.setInterest_id(rs.getInt("interest_id"));
					interest.setInterest_name(rs.getString("interest_name"));
					interest.setCategory_id(rs.getInt("category_id"));
					interest.setCategory_name(rs.getString("category_name"));
					user.getInterests().add(interest);
				}
				response.setData(new Gson().toJsonTree(user).getAsJsonObject());
			}else
				throw new Exception("User not active or doesnt exist");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println(new Gson().toJson(response));
		return new Gson().toJson(response);
	}

	public String updateUser(String data){
		ResponseObject response = new ResponseObject();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		User user = new User();
		DatabaseHelper database = new DatabaseHelper();
		JsonObject receivedData = new JsonParser().parse(data).getAsJsonObject();
		try{
			user.setId(receivedData.get("user_id").getAsInt());
			user.setName(receivedData.get("user_name").getAsString());
			user.setNumber(receivedData.get("user_number").getAsString());
			user.setEmail_id(receivedData.get("user_email_id").getAsString());
			user.setPassword(receivedData.get("user_password").getAsString());
			conn= database.getConnection();
			conn.setAutoCommit(false);
			String checkEmail = "Select u1.user_id from user u1 where u1.user_id <> ? and u1.user_email_id = ?";
			pstmt = conn.prepareStatement(checkEmail);
			pstmt.setInt(1, user.getId());
			pstmt.setString(2, user.getEmail_id());
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("User email already exists");
			String checkNum = "Select u1.user_id from user u1 where u1.user_id <> ? and u1.user_number = ?" ;
			pstmt = conn.prepareStatement(checkNum);
			pstmt.setInt(1, user.getId());
			pstmt.setString(2, user.getNumber());
			rs = pstmt.executeQuery();
			if(rs.next())
				throw new Exception("User number already exists");
			Type listType = new TypeToken<ArrayList<Interest>>(){}.getType();
			ArrayList<Interest> interestsNew = (ArrayList<Interest>) new Gson().fromJson(receivedData.get("interests").getAsJsonArray(), listType);
			String updateUser = "update user u set u.user_name = ? , u.user_email_id = ?"
					+ ", u.user_number = ?, u.user_password = ?,"
					+ " u.last_updated = ?  where u.user_id = ? and u.active = ?";
			pstmt = conn.prepareStatement(updateUser);
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getEmail_id());
			pstmt.setString(3, user.getNumber());
			pstmt.setString(4,user.getPassword());
			pstmt.setLong(5,System.currentTimeMillis());
			pstmt.setInt(6, user.getId());
			pstmt.setInt(7, 1);
			int row = pstmt.executeUpdate();
			if(row<=0)
				throw new Exception("Could not update user");
			String inactiveInterest = "update user_interests ui set ui.active=? where ui.user_id=?";
			pstmt = conn.prepareStatement(inactiveInterest);
			pstmt.setInt(1, 0);
			pstmt.setInt(2, user.getId());
			int r = pstmt.executeUpdate();
			String insertInterests = "insert into user_interest(user_id,interest_id,created_on,last_updated,active) values(?,?,?,?,?)"
					+"on duplicate key update active = values(active), last_updated=values(last_updated)";
			pstmt = conn.prepareStatement(insertInterests);
			for(Interest interest : interestsNew){
				pstmt.setInt(1, user.getId());
				pstmt.setInt(2, interest.getInterest_id());
				pstmt.setLong(3, System.currentTimeMillis());
				pstmt.setLong(4, System.currentTimeMillis());
				pstmt.setInt(5, 1);
				pstmt.addBatch();
			}
			int res[] = pstmt.executeBatch();
			for(int i : res)
				if(i<0)
					throw new Exception("Could not update Interests");
			String select = "select user_id,user_name,user_number,user_email_id,user_latitude,user_longitude from user u where u.user_id = ? and u.active = ?";
			pstmt = conn.prepareStatement(select);
			pstmt.setInt(1, user.getId());
			pstmt.setInt(2, 1);
			rs = pstmt.executeQuery();
			if(rs.next()){
				user.setId(rs.getInt("user_id"));
				user.setName(rs.getString("user_name"));
				user.setNumber(rs.getString("user_number"));
				user.setEmail_id(rs.getString("user_email_id"));
				user.setLatitude(rs.getBigDecimal("user_latitude"));
				user.setLongitude(rs.getBigDecimal("user_longitude"));
				select = "select * from user_interest_view uiv where uiv.user_id = ?";
				pstmt = conn.prepareStatement(select);
				pstmt.setInt(1, user.getId());
				rs = pstmt.executeQuery();
				Interest interest = new Interest();
				while(rs.next()){
					interest.setInterest_id(rs.getInt("interest_id"));
					interest.setInterest_name(rs.getString("interest_name"));
					interest.setCategory_id(rs.getInt("category_id"));
					interest.setCategory_name(rs.getString("category_name"));
					user.getInterests().add(interest);
				}
				response.setData(new Gson().toJsonTree(user).getAsJsonObject());
			}
			conn.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			try { if (rs != null) conn.rollback(); } catch (SQLException ef) {
				ef.printStackTrace();
			}
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}

	public String getNearbyUsers(String data){
		ResponseObject response = new ResponseObject();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		JsonObject receivedData = new JsonParser().parse(data).getAsJsonObject();
		String sql = "CALL nearby_users(?,?,?,?,?)";
		JsonArray users = new JsonArray();
		try(CallableStatement callStmt = conn.prepareCall(sql)){
			callStmt.setBigDecimal(1, receivedData.get("user_latitude").getAsBigDecimal());
			callStmt.setBigDecimal(2, receivedData.get("user_longitude").getAsBigDecimal());
			callStmt.setBigDecimal(3, receivedData.get("user_radius").getAsBigDecimal());
			callStmt.setBigDecimal(4, receivedData.get("user_dunit").getAsBigDecimal());
			callStmt.registerOutParameter(5, Types.REF_CURSOR);
			callStmt.executeUpdate();
			rs = (ResultSet) callStmt.getObject(5);
			while(rs.next()){
				JsonObject user = new JsonObject();
				user.addProperty("user_id",rs.getInt(1));
				user.addProperty("user_name",rs.getString(2));
				user.addProperty("user_email_id",rs.getString(3));
				user.addProperty("user_number",rs.getString(4));
				user.addProperty("user_latitude",rs.getBigDecimal(5));
				user.addProperty("user_longitude",rs.getBigDecimal(6));
				user.addProperty("user_distance",rs.getInt(7));
				users.add(user);
			}
			response.setData(users);
		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return new Gson().toJson(response);
	}

	public String getRecommendedUsers(String data){
		ResponseObject response = new ResponseObject();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		JsonObject receivedData = new JsonParser().parse(data).getAsJsonObject();
		try{
			
		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}
	
	public String addInterests(String data){
		ResponseObject response = new ResponseObject();
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		DatabaseHelper database = new DatabaseHelper();
		JsonObject receivedData = new JsonParser().parse(data).getAsJsonObject();
		try{
			int user_id = receivedData.get("user_id").getAsInt();
			String insertInterests = "insert into user_interest(user_id,interest_id,created_on,last_updated,active) values(?,?,?,?,?)";
			Type listType = new TypeToken<ArrayList<Interest>>(){}.getType();
			ArrayList<Interest> addInterests = (ArrayList<Interest>) new Gson().fromJson(receivedData.get("interests").getAsJsonArray(), listType);
			conn = database.getConnection();
			pstmt = conn.prepareStatement(insertInterests);
			for(Interest interest : addInterests){
				pstmt.setInt(1, user_id);
				pstmt.setInt(2, interest.getInterest_id());
				pstmt.setLong(3, System.currentTimeMillis());
				pstmt.setLong(4, System.currentTimeMillis());
				pstmt.setInt(5, 1);
				pstmt.addBatch();
			}
			int res[] = pstmt.executeBatch();
			for(int i : res)
				if(i<0)
					throw new Exception("Could not add Interests");
		}catch(Exception e){
			e.printStackTrace();
			response.setSuccess(false);
			response.setErrorMsg(e.getMessage());
			return new Gson().toJson(response);
		}finally{
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (pstmt != null) pstmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return new Gson().toJson(response);
	}
}

/***
 * 
 * Template for method
 * 
 * public String getNearbyUsers(String data){
 *		ResponseObject response = new ResponseObject();
 *		PreparedStatement pstmt = null;
 *		Connection conn = null;
 *		ResultSet rs = null;
 *		DatabaseHelper database = new DatabaseHelper();
 *		JsonObject receivedData = new JsonParser().parse(data).getAsJsonObject();
 *		return new Gson().toJson(response);
 *	}
 */ 
